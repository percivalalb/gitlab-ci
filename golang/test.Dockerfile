# Contains gcc & g++ libraries
FROM golang:1.18-bullseye

ARG TARGETOS TARGETARCH

RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg/ \
        GOOS=$TARGETOS \
        GOARCH=$TARGETARCH \
        go install gotest.tools/gotestsum@v1.8.1 \
        && go install github.com/boumenot/gocover-cobertura@v1.2.0
