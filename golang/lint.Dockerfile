# Contains gcc & g++ libraries
FROM golang:1.18-bullseye

ARG TARGETOS TARGETARCH

RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg/ \
        GOOS=$TARGETOS \
        GOARCH=$TARGETARCH \
        go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.46.2
