FROM gradle:7-jdk17

ARG TARGETOS TARGETARCH

ARG TARGETFORGE=1.19.2-43.1.24 

ADD https://maven.minecraftforge.net/net/minecraftforge/forge/${TARGETFORGE}/forge-${TARGETFORGE}-mdk.zip /forge-mdk.zip

RUN unzip /forge-mdk.zip

RUN --mount=type=cache,target=/home/gradle/.gradle \
       gradle assemble
